import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  BaseURL = environment.BaseURL
  

  constructor() { }
  
  
  async getBusinessProfile(id:string){
    
    let res = await fetch(this.BaseURL+'business/view/id/'+id,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getBusinessProfileGallery(business_id:string){
    
    let res = await fetch(this.BaseURL+'business/viewgallery/business_id/'+business_id,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }




  async updateAddress(formvalue){
    console.log(formvalue)
    let res = await fetch(this.BaseURL+'business/editAddress',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }


  async updateBusiness(formvalue){
    console.log(formvalue)
    let res = await fetch(this.BaseURL+'business/edit',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }


  async addPhoto(formvalue:{business_id:string,image: string, base64: string}){
    console.log()
    let res = await fetch(this.BaseURL+'business/addgallery',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }


  async removePhoto(formvalue:{business_id:string,gallery_id: string}){
    console.log()
    let res = await fetch(this.BaseURL+'business/removegallery',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }

  async setAsPrimary(formvalue:{business_id:string,gallery_id: string}){
    console.log()
    let res = await fetch(this.BaseURL+'business/removegallery',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }


  
}
