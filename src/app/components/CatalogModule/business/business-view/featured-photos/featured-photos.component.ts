import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-featured-photos',
  templateUrl: './featured-photos.component.html',
  styleUrls: ['./featured-photos.component.css']
})
export class FeaturedPhotosComponent implements OnInit {

  @Input() photos
  @Input() isAdmin 
  constructor() { }

  ngOnInit() {
  }

}
