import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/services/business/business.service';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/services/global/toast.service';



@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.css']
})
export class PhotoGalleryComponent implements OnInit {
  selectedFile: string;
  isUpload: boolean;
  business_id: string;
  title:'Photo Gallery'
  gallery: any;
  actionButton: string;
  isLoading: boolean;
  selectedGallery: any;
  selectedItem: any;
  
  constructor(
    private businessService: BusinessService,
    private toastSvc: ToastService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.selectedGallery=null
    this.selectedItem=null
     this.actionButton='Upload Photo'
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
      this.getBusinessProfileGallery(this.business_id)
      
    })
  }

  promoImageChanged(e){
    let target: any = event.target
    let file = target.files[0]
    var myReader:FileReader = new FileReader();
    myReader.onloadend = (e) => {
      let res:any = myReader.result
      console.log(file.name)
      //this.selectedFile=res
      console.log(res.split(',')[1])
      this.upload(file.name,res)

    }
    myReader.readAsDataURL(file)
  }


  async upload(name,file){
    this.isUpload=true
    this.actionButton='Uploading...'
    let formvalue={
      business_id:this.business_id,
      image:name,
      base64: file.split(',')[1],
    }
    try{  
      let response = await this.businessService.addPhoto(formvalue)
      console.log(response.gallery)
      this.isUpload=false
      this.actionButton='Upload Photo'
      this.gallery.unshift(response.gallery)
     
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }

  selectGallery(i,item){
    this.selectedItem=i
    this.selectedGallery=item
    console.log(this.selectedItem)
  }


  async removePhoto(gallery_id){
    let formvalue={
      business_id:this.business_id,
      gallery_id:gallery_id,
     
    }
    try{  
      let response = await this.businessService.removePhoto(formvalue)
      console.log(response.gallery)
      this.selectedGallery=null
      this.selectedItem=null
      this.gallery.splice(this.selectedItem,1)
      this.toastSvc.sendMessage("Photo Remove!")
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }

  async setAsPrimary(gallery_id){
    let formvalue={
      business_id:this.business_id,
      gallery_id:gallery_id,
     
    }
    try{  
      let response = await this.businessService.setAsPrimary(formvalue)
      console.log(response.gallery)
      this.toastSvc.sendMessage("Photo Remove!")
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }


  async getBusinessProfileGallery(business_id){
    this.isLoading=true;
    try{  
      let response = await this.businessService.getBusinessProfileGallery(business_id)
      console.log(response)
      this.gallery=response.gallery
      this.isLoading=false;
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }

  

}
