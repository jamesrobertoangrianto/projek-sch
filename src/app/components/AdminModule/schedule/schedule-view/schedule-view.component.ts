import { Component, OnInit } from '@angular/core';
import { ScheduleService } from 'src/app/services/schedule/schedule.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-schedule-view',
  templateUrl: './schedule-view.component.html',
  styleUrls: ['./schedule-view.component.css']
})
export class ScheduleViewComponent implements OnInit {
  isLoading: boolean;
  schedule: any;
  business_id: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private scheduleService : ScheduleService) { 
    
  }


  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("id")
      console.log(this.business_id)
      this.getScheduleById(this.business_id)
     
    })
    
  }

  async getScheduleById(id){
    try{
      this.isLoading=true
      let response = await this.scheduleService.getScheduleById(id)  
     this.schedule=response.schedule

      // this.from_date=response.from_date
      console.log(response)
   
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }

}
