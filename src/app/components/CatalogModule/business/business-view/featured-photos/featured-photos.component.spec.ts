import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedPhotosComponent } from './featured-photos.component';

describe('FeaturedPhotosComponent', () => {
  let component: FeaturedPhotosComponent;
  let fixture: ComponentFixture<FeaturedPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
