import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tab-bar',
  templateUrl: './tab-bar.component.html',
  styleUrls: ['./tab-bar.component.css']
})
export class TabBarComponent implements OnInit {
@Input() items : string
title = 'Account'
item : any;
  selected: any;
  active_tab: string;


  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getActiveTab()
  }

  getActiveTab(){
    this.route.queryParamMap.subscribe(queryParams => {
      let tab = this.route.snapshot.queryParamMap.get('active_tab')
      if(tab){
        this.active_tab = tab
      }
      else{
        this.active_tab='1'
      }
    })
  }

}
