import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, DoBootstrap} from '@angular/core';
import { AppComponent } from './app.component';
// import { FcmComponent } from './modules/fcm/fcm.component';
// import {createCustomElement} from '@angular/elements'
import { HomepageComponent } from './components/CatalogModule/homepage/homepage.component';
import { AppRoutingModule } from './app.routing.module';
import { TopNavigationBarComponent } from './shared/top-navigation-bar/top-navigation-bar.component';
import { PopupComponent } from './shared/popup/popup.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ImageSliderComponent } from './shared/image-slider/image-slider.component';
import { ToastComponent } from './shared/toast/toast.component';
import { LoginComponent } from './components/AuthModule/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoadingComponent } from './shared/loading/loading.component';
import { BottomNavigationBarComponent } from './shared/bottom-navigation-bar/bottom-navigation-bar.component';
import { AgmCoreModule } from '@agm/core';
import { ReviewListComponent } from './components/ReviewModule/review-list/review-list.component';
import { AddReviewComponent } from './components/ReviewModule/review-list/add-review/add-review.component';
import { ReviewItemComponent } from './shared/card/review-item/review-item.component';
import { CampaignItemComponent } from './shared/card/campaign-item/campaign-item.component';
import { GlobalFooterComponent } from './shared/global-footer/global-footer.component';
import { SocialLoginComponent } from './shared/block/social-login/social-login.component';
import { StoreComponent } from './shared/card/store/store.component';
import { StoreFooterInformationComponent } from './shared/card/store-footer-information/store-footer-information.component';
import { BookingComponent } from './components/AdminModule/booking/booking.component';
import { ScheduleComponent } from './components/AdminModule/schedule/schedule.component';
import { ServiceComponent } from './components/AdminModule/service/service.component';
import { BookingViewComponent } from './components/AdminModule/booking/booking-view/booking-view.component';
import { CalendarItemComponent } from './shared/card/calendar-item/calendar-item.component';
import { ScheduleViewComponent } from './components/AdminModule/schedule/schedule-view/schedule-view.component';
import { BusinessProfileComponent } from './components/AdminModule/business-profile/business-profile.component';
import { TabBarComponent } from './shared/tab-bar/tab-bar.component';
import { FeaturedScheduleComponent } from './components/AdminModule/business-profile/featured-schedule/featured-schedule.component';
import { BusinessComponent } from './components/CatalogModule/business/business.component';
import { BusinessViewComponent } from './components/CatalogModule/business/business-view/business-view.component';
import { ServiceViewComponent } from './components/AdminModule/service/service-view/service-view.component';
import { FeaturedServiceComponent } from './components/CatalogModule/business/business-view/featured-service/featured-service.component';
import { FeaturedPhotosComponent } from './components/CatalogModule/business/business-view/featured-photos/featured-photos.component';
import { DashboardComponent } from './components/AdminModule/dashboard/dashboard.component';
import { BusinessLocationComponent } from './components/CatalogModule/business/business-view/business-location/business-location.component';
import { BookingAddComponent } from './components/AdminModule/booking/booking-add/booking-add.component';
import { BusinessBookingComponent } from './components/CatalogModule/business/business-view/business-booking/business-booking.component';
import { CreateComponent } from './components/AuthModule/create/create.component';
import { DesktopSidebarComponent } from './shared/desktop-sidebar/desktop-sidebar.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ListTilePlaceholderComponent } from './shared/placeholder/list-tile-placeholder/list-tile-placeholder.component';
import { PhotoGalleryComponent } from './components/AdminModule/business-profile/photo-gallery/photo-gallery.component';
import { CalendarComponent } from './shared/calendar/calendar.component';
import { CategoryViewComponent } from './components/AdminModule/service/category-view/category-view.component';
import { CustomerComponent } from './components/AdminModule/customer/customer.component';
import { CustomerViewComponent } from './components/AdminModule/customer/customer-view/customer-view.component';


@NgModule({
  declarations: [
    AppComponent,
    // FcmComponent,
    HomepageComponent,
    TopNavigationBarComponent,
    PopupComponent,
    ImageSliderComponent,
    ToastComponent,
    LoginComponent,
    CreateComponent,
    LoadingComponent,
    BottomNavigationBarComponent,
    ReviewListComponent,
    AddReviewComponent,
    ReviewItemComponent,
    CampaignItemComponent,
    GlobalFooterComponent,
    SocialLoginComponent,
    StoreComponent,
    StoreFooterInformationComponent,
    BookingComponent,
    ScheduleComponent,
    ServiceComponent,
    BookingViewComponent,
    CalendarItemComponent,
    ScheduleViewComponent,
    BusinessProfileComponent,
    TabBarComponent,
    FeaturedScheduleComponent,
    BusinessComponent,
    BusinessViewComponent,
    ServiceViewComponent,
    FeaturedServiceComponent,
    FeaturedPhotosComponent,
    DashboardComponent,
    BusinessLocationComponent,
    BookingAddComponent,
    BusinessBookingComponent,
    DesktopSidebarComponent,
    ListTilePlaceholderComponent,
    PhotoGalleryComponent,
    CalendarComponent,
    CategoryViewComponent,
    CustomerComponent,
    CustomerViewComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAmlnIAu52yOhMzIPFGxSsDaHB9TXmqhQk',
      libraries: ['places']
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[
    // FcmComponent,
    // FeaturedJournalComponent,
    // EditorsPickComponent,
    // FeaturedCategoriesComponent,
    // FeaturedBrandComponent
  ]
})

export class AppModule implements DoBootstrap{
  constructor(private injector: Injector){}

  ngDoBootstrap(){
    // const customModule={
    //   'notification-module' : createCustomElement(FcmComponent, {injector: this.injector}),
    // }

  
    // for(let k in customModule){
    //   let v=customModule[k]
    //   if (!customElements.get(k)) {
    //     customElements.define(k, v);
    //   }
    // }

   
  }
}
