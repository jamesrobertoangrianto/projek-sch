import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/global/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterextService } from 'src/app/services/routerext/routerext.service';
import { formError } from 'src/app/const/formError';
import { AdminAuthService } from 'src/app/services/adminAuth/admin-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formErrorMsg = formError

  loginForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email]
    }),
    password: new FormControl(null,{ 
      validators: [Validators.required]
    })
  })
  
  isLoading: Boolean
  hasLoggedInFB: Boolean
  isLoggedIn: boolean

  title = 'Login'
  isLogin: boolean;
  business_id: any;
  returnUrl: string;
  constructor(
    private adminAuthService: AdminAuthService,
    private toastSvc: ToastService,
    private router: Router,
    private route: ActivatedRoute,
    private routerExtSvc: RouterextService,
    private ngZone: NgZone
  ) { }

  async ngOnInit() {
    this.route.queryParamMap.subscribe(queryParams => {
      this.returnUrl = queryParams.get("returnUrl")
      console.log(this.returnUrl)
    })

    let res = this.adminAuthService.getLocalCustomerSession()
    //console.log(res)
    if(res!==null){
      this.isLogin=true
    }


   

  }
  
  

  async login(){
    if(this.loginForm.valid){
      try{
        this.isLoading=true

        let res =await this.adminAuthService.login(this.loginForm.value)
        console.log(res)

        let customerSession = this.adminAuthService.getLocalCustomerSession()
        console.log(customerSession)

        if(res.customer.business_id){
          this.business_id = res.customer.business_id
          this.redirect(this.business_id)
        }
       
      }catch(e){
        this.toastSvc.sendMessage(e)
      }finally{
        this.isLoading=false
      }
    }
  }


  redirect(id){

    if(this.returnUrl){
      this.router.navigate([this.returnUrl],{
        replaceUrl: true
      })
    }
    else{
      this.router.navigate(['/admin/dashboard/'+id],{
        replaceUrl: true
      })
    }

  
  }
}
