import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { formError, validPhoneNumber } from 'src/app/const/formError';
import { BookingService } from 'src/app/services/booking/booking.service';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { formatDate } from '@angular/common';
import { ToastService } from 'src/app/services/global/toast.service';


@Component({
  selector: 'app-business-booking',
  templateUrl: './business-booking.component.html',
  styleUrls: ['./business-booking.component.css']
})
export class BusinessBookingComponent implements OnInit {
  active_step: any;
  isLoading: boolean;
  product: any;
  faChevronRight=faChevronRight;


  modal={
    selectTime:false,
    selectDate:false,
    viewDetails:false,
  }
  
  formErrorMsg=formError
  isShowBookingForm: boolean;
  isUpdating: boolean;
  update_message: string;
  update_booking_id: string;
  title: string;
  product_id : string;
  selected_product_id: any;

  bookingForm = new FormGroup({
    business_id: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    product_id: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    customer_email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email]
    }),
    customer_name: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    from_date: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    from_time: new FormControl(null,{ 
      validators: [Validators.required]
    }),

    customer_phone: new FormControl(null,{ 
      validators: [Validators.required, validPhoneNumber]
    }),
   
  })
  booking_id: string;
  business_id: string;
  isUpdate: boolean;
  from_date: any;
  from_time: any;
  calendar: any;
  time: any[];
  currentMonth: any;
  date: Date;
  currentdate: Date;
  selectedItem: any;
 

  constructor(
    private productService : ProductService,
    private bookingService: BookingService,
    private route: ActivatedRoute,
    private router: Router,
    private toastSvc: ToastService,
  ) { }

  ngOnInit() {
    var year=2020
    var month=1
    let firstDay = (new Date(year, month)).getDay();
    
    console.log(firstDay)
    console.log('ad')


    console.log(this.daysInMonth(3,2020))
    console.log('ad')
    
    this.isShowBookingForm=true
    
    this.showTime()
    
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
      this.getServiceList(this.business_id )
      this.bookingForm.controls['business_id'].setValue(this.business_id);
   
    })
    
    
    this.route.queryParamMap.subscribe(queryParams => {
      this.active_step = queryParams.get("step")
      this.product_id = queryParams.get("product_id")
      this.setTitle(this.active_step)
      console.log( this.product_id)
      this.bookingForm.controls['product_id'].setValue(this.product_id);
   
    })

  }

  daysInMonth(iMonth, iYear) 
  { 
      return 32 - new Date(iYear, iMonth, 32).getDate();
  }

  setTitle(step){
    if(step ==1 || step==null){
      this.title='Select Service'
    }
    if(step==2){
      this.title='Add Booking Details'
    }
   
  }

  viewDetails(item){
    console.log(item)
    this.selectedItem=item
    this.openModal('viewDetails')
  }

  selectTime(time){
    
    this.from_time=formatDate(time, 'HH:mm', 'en_US')
    console.log(this.from_time)
    this.bookingForm.controls['from_time'].setValue(this.from_time);
    this.closeModal('selectTime')
  }

  
  selectDate(date){
    this.from_date= formatDate(date, 'yyyy-MM-dd', 'en_US') 
    console.log(this.from_date)
    this.bookingForm.controls['from_date'].setValue(this.from_date);
    this.closeModal('selectDate')
  }



  showTime(){
    var today = new Date();
    var currentMonth = today.getMonth();
    var currentYear = today.getFullYear();

    var openTime = new Date(today.setHours(10,0,0))
    var closeTime = new Date(today.setHours(20,0,0))

    var arr = new Array();
    var dt = new Date(openTime);
    while (dt <= closeTime) {
        arr.push(new Date(dt));
        dt.setHours(dt.getHours() + 1);
    }
    this.time=arr
    console.log( arr)

  }

  async getServiceList(id){
    try{
      this.isLoading=true
      let response = await this.productService.getProductList(id,1)  
      this.product=response.product

      // this.from_date=response.from_date
      console.log(this.product)
   
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }


  //step 2
  async addBooking(){
    

    if(this.bookingForm.valid){
      try{
          this.isUpdate=true
        let response = await this.bookingService.addBooking(this.bookingForm.value)
        console.log(response)
        this.booking_id=response.booking.increment_id
        this.isUpdate=false
       
    
      }catch(e){
        console.log(e)
      }finally{
        this.isShowBookingForm=false
      }
    }
    else{
      this.toastSvc.sendMessage("Please check your booking details")
    }


      
      
    
  }



  openModal(name){
    this.modal[name]=true
    
  }

  closeModal(name){
    this.modal[name] = false
  }

 

}
