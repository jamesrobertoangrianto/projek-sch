import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from 'src/app/services/global/toast.service';
import { formError,validPhoneNumber } from 'src/app/const/formError';

@Component({
  selector: 'app-service-view',
  templateUrl: './service-view.component.html',
  styleUrls: ['./service-view.component.css']
})
export class ServiceViewComponent implements OnInit {
  isLoading: boolean;
  product: any;
  formErrorMsg=formError

  durations=[
    {
      value:0,
      label:'30 Minutes',
    },
    {
      value:1,
      label:'1 Hour',
    },
    {
      value:2,
      label:'2 Hours',
    },
    {
      value:3,
      label:'3 Hours',
    },
    {
      value:4,
      label:'4 Hours',
    },
  ]


  productForm = new FormGroup({
    business_id: new FormControl(null),
    id: new FormControl(null),

    name: new FormControl(null,{
      validators: [Validators.required]
    }),

    price: new FormControl(null,{
      validators: [Validators.required,Validators.pattern(new RegExp("[0-9 ]"))]
    }),

    special_price: new FormControl(null,{
      validators: [Validators.pattern(new RegExp("[0-9 ]"))]
    }),

    short_description: new FormControl(null,{
      validators: [Validators.required]
    }),

    description: new FormControl(null),


    status: new FormControl(null,{
      validators: [Validators.required]
    }),
    service_duration: new FormControl(null,{
      validators: [Validators.required]
    }),

 
  })
  product_id: any;
  business_id: string;
  isUpdate: boolean;
  updateButton: string = 'Update'
  isSpecialPrice: boolean;
  routerLink: string;

  
  constructor(
    private productService : ProductService,
    private route: ActivatedRoute,
    private router: Router,
    private toastSvc: ToastService,
  ) { }

  ngOnInit() {

    

    this.isSpecialPrice=false
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      this.routerLink='/admin/service/'+this.business_id
 
    })

   
    this.route.paramMap.subscribe(params => {
      this.product_id = params.get("id")
      console.log(this.product_id)

      this.getServiceById(this.product_id)

      
    })
  }
  
  toggleCheckbox(id,event){
   console.log(id,event.target.checked)

    if(id == 'special_price'){
      this.isSpecialPrice=!this.isSpecialPrice
      if(this.isSpecialPrice==false){
        this.productForm.controls['special_price'].setValue(null);
      }
      
    }
    else{
      this.productForm.controls[id].setValue(event.target.checked);
    }
    
    
  }


  async getServiceById(id){
    try{
      this.isLoading=true
      let response = await this.productService.getProductById(id)  
      this.product=response.product

      // this.from_date=response.from_date
      console.log(response)
      if(response.product.special_price!==null){
        this.isSpecialPrice=true
      }
      
      this.productForm.patchValue(this.product)

    //  this.productForm.controls['service_duration'].setValue(2);
      
   
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }



  async updateProduct(){
    console.log(this.productForm.value)  
    if(this.productForm.valid){
      try{
        this.isUpdate=true
        this.updateButton='Please Wait'
        let res =  await this.productService.updateProduct(this.productForm.value)
       
        
        this.isUpdate=false
        this.updateButton='Update'
  
       // this.router.navigate([ '/admin/service/'+this.business_id],{replaceUrl:true})
        this.toastSvc.sendMessage("Update success!")
 
      
      }catch(e){
        // this.toastSvc.sendMessage(e)
      }finally{
        this.isLoading=false
        
      }
    }
    else{
      this.toastSvc.sendMessage("Please check your prpduct details!")
    }
    
  }




}
