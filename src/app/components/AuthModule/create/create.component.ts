import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { confirmPasswordValidator, formError } from 'src/app/const/formError';
import { RouterextService } from 'src/app/services/routerext/routerext.service';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/services/global/toast.service';
import { AdminAuthService } from 'src/app/services/adminAuth/admin-auth.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  title  = 'Create an Account'
  formErrorMsg=formError
  
  registerForm = new FormGroup({
    account_type: new FormControl('business'),
    email: new FormControl(null,{
      validators: [Validators.required, Validators.email]
    }),
    firstname: new FormControl(null,{
      validators: [Validators.required]
    }),
    lastname: new FormControl(null,{
      validators: [Validators.required]
    }),
    password: new FormControl(null,{
      validators: [Validators.required]
    }),
    confirm_password: new FormControl(null,{
      validators: [Validators.required]
    })
  },{
  })
  isLoading: boolean;


  constructor(

    private ngZone: NgZone,
    private routerExtSvc: RouterextService,
    private router: Router,
    private toastSvc: ToastService,
    private adminAuthService: AdminAuthService,
  ) { }

  ngOnInit() {
 
  }

  async createAccount(){
   
    try{
      this.isLoading=true
      let res = await this.adminAuthService.register(this.registerForm.value)
      console.log(res.customer.business_id)

      let customerSession = this.adminAuthService.getLocalCustomerSession()
      console.log(customerSession)
      
      this.router.navigate(['/admin/dashboard/'+res.customer.business_id],{
        replaceUrl: true
      })
    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.isLoading=false
    }
  }

  onSuccessFacebookLogin(res){
    if(res) this.redirect()
  }

  redirect(){
    this.ngZone.run(()=>{
      console.log("Previous url is " + this.routerExtSvc.getPreviousUrl())
      this.router.navigate([this.routerExtSvc.getPreviousUrl()],{
        replaceUrl: true
      })
    })
  }

}
