import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './components/CatalogModule/homepage/homepage.component';
import { BookingComponent } from './components/AdminModule/booking/booking.component';
import { ScheduleComponent } from './components/AdminModule/schedule/schedule.component';
import { ServiceComponent } from './components/AdminModule/service/service.component';
import { BookingViewComponent } from './components/AdminModule/booking/booking-view/booking-view.component';
import { ScheduleViewComponent } from './components/AdminModule/schedule/schedule-view/schedule-view.component';
import { BusinessProfileComponent } from './components/AdminModule/business-profile/business-profile.component';
import { BusinessComponent } from './components/CatalogModule/business/business.component';
import { BusinessViewComponent } from './components/CatalogModule/business/business-view/business-view.component';
import { ServiceViewComponent } from './components/AdminModule/service/service-view/service-view.component';
import { DashboardComponent } from './components/AdminModule/dashboard/dashboard.component';
import { BookingAddComponent } from './components/AdminModule/booking/booking-add/booking-add.component';
import { BusinessBookingComponent } from './components/CatalogModule/business/business-view/business-booking/business-booking.component';
import { AdminAuthGuard } from './components/AuthModule/admin-guard.guard';
import { LoginComponent } from './components/AuthModule/login/login.component';
import { CreateComponent } from './components/AuthModule/create/create.component';
import { PhotoGalleryComponent } from './components/AdminModule/business-profile/photo-gallery/photo-gallery.component';
import { CategoryViewComponent } from './components/AdminModule/service/category-view/category-view.component';
import { CustomerComponent } from './components/AdminModule/customer/customer.component';
import { CustomerViewComponent } from './components/AdminModule/customer/customer-view/customer-view.component';

const routes: Routes=[
    {
        path: '', redirectTo:'homepage', pathMatch: 'full'
    },
    {
        path: 'homepage', component:  HomepageComponent
    },

    
    
    

    {
        path: 'auth', children:[
            {
                path: 'login', component: LoginComponent,
            },
            {
                path: 'register', component: CreateComponent,
            },
           
        ],
    },


    //ADMIN MODULE,
    {
        path: 'admin',canActivate:[AdminAuthGuard], children:[
            {
                path: 'dashboard', children:[
                    {
                        path: ':business_id', component: DashboardComponent,
                    },
                    {
                        path: '**', redirectTo:'', pathMatch: 'full'
                    }
                ],
            },
            {
                path: 'profile', children:[
                    {
                        path: ':business_id', component: BusinessProfileComponent,
                    },
                    {
                        path: ':business_id/photo-gallery', component: PhotoGalleryComponent
                    },
                   
                    {
                        path: '**', redirectTo:'', pathMatch: 'full'
                    }
                ],
            },
            {
                path: 'booking', children:[
                    {
                        path: ':business_id', component: BookingComponent,
                    },
                    {
                        path: ':business_id/view/booking_id/:id', component: BookingViewComponent
                    },
                    {
                        path: ':business_id/add', component: BookingAddComponent
                    },
                    {
                        path: '**', redirectTo:'', pathMatch: 'full'
                    }
                ],
            },
            {
                path: 'schedule', children:[
                    {
                        path: ':business_id', component: ScheduleComponent,
                    },
                    {
                        path: ':business_id/view/schedule_id/:id', component: ScheduleViewComponent
                    },
                    {
                        path: '**', redirectTo:'', pathMatch: 'full'
                    }
                ],
            },
            {
                path: 'customer', children:[
                    {
                        path: ':business_id', component: CustomerComponent,
                    },
                    {
                        path: ':business_id/view/customer_id/:id', component: CustomerViewComponent
                    },
                    {
                        path: ':business_id/add', component: CustomerViewComponent
                    },
                    {
                        path: '**', redirectTo:'', pathMatch: 'full'
                    }
                ],
            },
            {
                path: 'service', children:[
                    {
                        path: ':business_id', component: ServiceComponent,
                    },
                    {
                        path: ':business_id/view/service_id/:id', component: ServiceViewComponent
                    },
                    {
                        path: ':business_id/view/category_id/:id', component: CategoryViewComponent
                    },
                    {
                        path: '**', redirectTo:'', pathMatch: 'full'
                    }
                ],
            },
            {
                path: '**', redirectTo:'', pathMatch: 'full'
            }
        ],
    },

 

    
  
    {
        path: 'business', children:[
            {
                path:'', component: BusinessComponent,
            },
            {
                path: ':business_id', component: BusinessViewComponent
            },
            {
                path: ':business_id/booking', component: BusinessBookingComponent
            },
            {
                path: '**', redirectTo:'', pathMatch: 'full'
            }
            
        ]
    },
   
    {
        path: '**', redirectTo: 'homepage', pathMatch: 'full'
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes,{
        scrollPositionRestoration: 'enabled',
    }) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}