import { Component, OnInit } from '@angular/core';
import {  faThList, faStore, faCalendarWeek, faReceipt ,faPercent,faThLarge,faUserCircle,faHome,faTags, faFileInvoiceDollar } from '@fortawesome/free-solid-svg-icons';
import { AdminAuthService } from 'src/app/services/adminAuth/admin-auth.service';

@Component({
  selector: 'app-desktop-sidebar',
  templateUrl: './desktop-sidebar.component.html',
  styleUrls: ['./desktop-sidebar.component.css']
})
export class DesktopSidebarComponent implements OnInit {

  faUserCircle = faUserCircle
  faHome = faHome
  faStore=faStore
  faThList=faThList
  faFileInvoiceDollar = faFileInvoiceDollar
  faTags= faTags
  faCalendarWeek=faCalendarWeek
  faReceipt = faReceipt
  faThLarge= faThLarge
  faPercent =faPercent
  customer_id: any;
  business_id: any;
  constructor(
    private adminAuthService :AdminAuthService
  ) { }

  ngOnInit() {
   let res = this.adminAuthService.getLocalCustomerSession()
   console.log(res.customer.business_id)
   this.business_id=res.customer.business_id


   
  }

}
