import { FormControl, Form, FormGroup,Validators } from '@angular/forms'

export const formError={
    'required': "This field is required",
    'phone': "Not a valid phone number",
    'price': "Not a valid price",
    'rating': "Rating must not be zero",
    'email': "This is not a valid email",
    'confirmPassword': "Password confirmation is not same"
}

export function validPhoneNumber(e: FormControl): any{
    return isNaN(e.value) ? {'phone': true} : null
}







export function noZeroStarRating(e: FormControl): any{
    if(e.value == null) return {'rating': true}
    else if(e.value == 0) return {'rating': true}
    else return null
}

export function confirmPasswordValidator(e: FormGroup){
    console.log(e.controls.password.value)
    console.log(e.controls.confirm_password.value)

    if(e.controls.password.value == e.controls.confirm_password) return null
    else return {'confirmPassword': true}
}