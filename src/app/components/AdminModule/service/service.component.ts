import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { Router, ActivatedRoute } from '@angular/router';

import {faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  faChevronRight=faChevronRight
  title='My Service'


  tabs = [
    {
      id : '1',
      name : 'Product',
     
      // linkPath : '/admin/service',
    },
    {
      id : '2',
      name : 'Category',
      
      // linkPath : '/admin/service',
    },
   
   
  ]
  isLoading: boolean;
  service: any;
  product: any;
  business_id: string;
  booking_id: string;
  product_id: string;
  isUpdate: boolean;
  actionButton: string = '+ Add New Product'
  active_tab: string;
  category: any;

  constructor(
    private productService : ProductService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
      this.getServiceList(this.business_id)
      this.getCategoryList(this.business_id)
    })
    this.getActiveTab()
  
  }

  getActiveTab(){
    this.route.queryParamMap.subscribe(queryParams => {
      let tab = this.route.snapshot.queryParamMap.get('active_tab')
      if(tab){
        this.active_tab = tab
      }
      else{
        this.active_tab='1'
      }
    })
  }



  async getServiceList(id){
    let isAdmin=1
    try{
      this.isLoading=true
      let response = await this.productService.getProductList(id,0)  
      this.product=response.product

      // this.from_date=response.from_date
      console.log(this.product)
      this.isLoading=false
   
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }

  async getCategoryList(id){
    let isAdmin=1
    try{
      this.isLoading=true
      let response = await this.productService.getCategoryList(id,0)  
      this.category=response.category

      // this.from_date=response.from_date
      console.log(this.category)
      this.isLoading=false
   
    }catch(e){
      console.log(e)
    }finally{
     
    }
  }



  async addProduct(){
  
    try{
      this.isUpdate=true;
      this.actionButton='Please Wait'
      let response = await this.productService.addProduct(this.business_id)
      console.log(response)
      this.product_id=response.product.id

     this.router.navigate([ '/admin/service/'+this.business_id+'/view/service_id/'+  this.product_id],{replaceUrl:true})
     this.isUpdate=false;
     this.actionButton='+ Add New Product'
     // this.router.navigate(['/admin/service/view/service_id/'])
   
    }catch(e){
      console.log(e)
    }finally{
     
    }
    
  }

  async addCategory(){
  
    try{
      this.isUpdate=true;
      this.actionButton='Please Wait'
      let response = await this.productService.addCategory(this.business_id)
      console.log(response)
      this.product_id=response.product.id

     this.router.navigate([ '/admin/service/'+this.business_id+'/view/category_id/'+  this.product_id],{replaceUrl:true})
     this.isUpdate=false;
     this.actionButton='Add New Service'
     // this.router.navigate(['/admin/service/view/service_id/'])
   
    }catch(e){
      console.log(e)
    }finally{
     
    }
    
  }


  async removeProduct(product_id,i){
   
    if(confirm("Are you sure to delete?")) {
      this.product.splice(i,1)
      try{
      let response = await this.productService.removeProduct(this.business_id,product_id)
      console.log(response)
     // this.router.navigate(['/admin/service/view/service_id/'])
    }catch(e){
      console.log(e)
    }finally{
     
    }
    }
    
   
    
    
  }

  

}
