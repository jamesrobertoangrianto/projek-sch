import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/services/business/business.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BookingService } from 'src/app/services/booking/booking.service';
import { ProductService } from 'src/app/services/product/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { faPhone,faCommentAlt, faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import {  formError } from 'src/app/const/formError';

@Component({
  selector: 'app-business-view',
  templateUrl: './business-view.component.html',
  styleUrls: ['./business-view.component.css']
})
export class BusinessViewComponent implements OnInit {
  isLoading: boolean;
  business: any;
  faCheckCircles=faCheckCircle
  formErrorMsg=formError
  faCommentAlt=faCommentAlt
  faPhone=faPhone
  modal={
    bookingModal: false,
    cartModal: false
  }

  


  addProductForm = new FormGroup({
    names: new FormControl(null,{ 
      validators: [Validators.required]
    }),
   
   
  })

  toastSvc: any;
  booking_source: any;

  bookingForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email]
    }),
    name: new FormControl(null,{ 
      validators: [Validators.required]
    }),

    telephone: new FormControl(null,{ 
      validators: [Validators.required]
    }),
   
  })


  
  isShowBookingForm: boolean;
  product: any;
  setSelectedProduct: any;
  update_message: string;
  update_booking_id: string;
  booking_success: any;
  isUpdating: boolean;
  showServiceList: boolean;
  business_id: string;
  
  

  constructor(
    private businesService: BusinessService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    
    
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      //console.log(this.business_id)
      this.getBusinessProfile(this.business_id)
    })
    

  }
  

  async getBusinessProfile(id){
    try{
      this.isLoading=true
      let response = await this.businesService.getBusinessProfile(id)  
      this.business=response.business
      console.log(this.business)
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }



  openModal(name){
    this.modal[name]=true
  }

  closeModal(name){
    this.modal[name]= false
  }

 


}
