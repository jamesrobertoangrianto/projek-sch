import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  BaseURL = environment.BaseURL
  

  constructor() { }
  
  
  async getBookingList(id:string,params:string){
    
    let res = await fetch(this.BaseURL+'booking/view/business_id/'+id+params,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getBookingById(id:string){ 
    let res = await fetch(this.BaseURL+'booking/view/id/'+id,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  // async changeStatus(formvalue){
  //   let res = await fetch('https://filotropic.com/wia-module/booking/edit',{
  //     method: "POST",
  //     body: JSON.stringify(formvalue)
  //   })
  //   let data = await res.json()
  //   if(data.status_code != 200) throw data.message_dialog
  //   else{
  //     return data
  //   }
  // }

  async changeStatus(id,status){
    console.log(id,status)
    let res = await fetch(this.BaseURL+'booking/edit/id/'+id+'/status/'+status,{
      method: "POST",
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }



 
  async updateSchedule(formvalue){
    console.log(formvalue)
    let res = await fetch(this.BaseURL+'booking/editSchedule',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }


   
  // async addBooking(formvalue){
  //   let res = await fetch(this.BaseURL+'booking/add',{
  //     method: "POST",
  //     body: JSON.stringify(formvalue)
  //   })
  //   let data = await res.json()
  //   //console.log(data)
  //   if(data.status_code != 200) throw data.message_dialog
  //   else{
  //     return data
  //   }
  // }

  async addBooking(formvalue){
    console.log(formvalue)
    let res = await fetch(this.BaseURL+'booking/add/',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }




  
}
