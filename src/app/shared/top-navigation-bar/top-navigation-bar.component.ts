import { Component, OnInit, Input, OnChanges, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { RouterextService } from 'src/app/services/routerext/routerext.service';
import { faArrowLeft, faTimes,faChevronLeft,faSearch,faCommentDots,faShoppingBag,faUserCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'app-top-navigation-bar',
  templateUrl: './top-navigation-bar.component.html',
  styleUrls: ['./top-navigation-bar.component.css']
})
export class TopNavigationBarComponent implements OnInit {
  faArrowLefts = faArrowLeft;
  faSearch = faSearch;
  faTimes=faTimes
  faCommentDots = faCommentDots;
  faShoppingBag = faShoppingBag;
  faUserCircle=faUserCircle;
  faHelp = faInfoCircle

  @Input() transparant: boolean 
  @Input() title = null || '' ;
  @Input() routerLink?: string
  @Input() noBackButton?: boolean


  @Input() border: boolean
  @Input() isPop: boolean
  



  

  @Input() isAdmin?: boolean

  top: any;

  numberOfItems: any
  home: boolean;
  isSticky: boolean;


  constructor(
    public routerExtSvc: RouterextService,
    public router: Router,
    private ngLocation: Location,
    private ngZone: NgZone
  ){

  }

  ngOnInit() {    
    if(this.router.url == '/admin' || this.router.url == '/admin'){
      this.home=true
    }

    


    window.onscroll = ()=>{
      let navbar = document.querySelector("app-top-navigation-bar")
      if(navbar){
        let params=navbar.getBoundingClientRect()
        if (window.pageYOffset > params.height*1.1) {
          this.isSticky=true
        } else {
          this.isSticky=false
        }
      }
    }
    
  }

 
  navigate(){
    let state: any = this.ngLocation.getState()

    if(this.routerLink){
      this.ngLocation.replaceState(this.routerLink)
    }
    
    else this.ngLocation.back()
  }

}
