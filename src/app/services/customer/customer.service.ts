import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  BaseURL = environment.BaseURL
  constructor() { }

  async getCustomerList(id:string){
  
    let res = await fetch(this.BaseURL+'customer/viewbusinesscustomer/business_id/'+id,{
      method: "GET",
      
    })
    let data = await res.json()
    return data
  }
  async getCustomerDetails(business_id:string,customer_id:string){
  
    let res = await fetch(this.BaseURL+'customer/viewbusinesscustomer/business_id/'+business_id+'/customer_id/'+customer_id,{
      method: "GET",
      
    })
    let data = await res.json()
    return data
  }

  async updateCustomer(formvalue){
    //console.log(formvalue)
    let res = await fetch(this.BaseURL+'customer/addbusinesscustomer/',{
      method: "POST",
      body: JSON.stringify(formvalue)
     
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }



  


}
