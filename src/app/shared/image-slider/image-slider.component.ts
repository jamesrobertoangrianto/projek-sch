import { Component, OnInit, Input, OnChanges } from '@angular/core';
import Glide from '@glidejs/glide'

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.css']
})

export class ImageSliderComponent implements OnChanges {
  @Input('images') images:any
  glide: Glide

  constructor() { }

  ngOnChanges() {
    var slider = document.getElementById('productSlide')
    
    if(this.images == null) return
    for(let item of this.images){
      let div = document.createElement('div')
      div.className="glide__slide"

      let markup=
      `<div class="medium-12 columns text-center">
        <div class="image-container">
          <div class="product-image padding-center">
            <img height="263" width="350" src="${item.image_2x}">
          </div>
        </div>
      </div>`

      div.innerHTML=markup
      slider.appendChild(div)
    }
    this.glide = new Glide('.glide').mount()
  }

}
