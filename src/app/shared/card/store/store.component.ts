import { Component, OnInit, Input } from '@angular/core';
import { faCircle , faEllipsisV,faShareAlt, faStar, faBell,faCommentAlt,faPhone,faMap } from '@fortawesome/free-solid-svg-icons';
import { ToastService } from 'src/app/services/global/toast.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  @Input() business :any
  @Input() isAdmin :boolean

  modal={
    share:false,
    
  }
  business_id: any;
  constructor( private toastSvc: ToastService,    private route: ActivatedRoute,) { }
  faCommentAlt=faCommentAlt
  faPhone= faPhone
  faMap=faMap
  faDotCircle=faCircle
  faStar=faStar
  faShare=faShareAlt
  faEllipsisV=faEllipsisV
  
  placeholder='https://via.placeholder.com/700x350';
  faBell=faBell
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
     
    })
  }

  copyInputMessage(val: string){
    if(val=='profile'){
      var url='https://getnotify.id/business/'+this.business_id
    }
    if(val=='price'){
      var url='https://getnotify.id/business/'+this.business_id+'/booking'
    }
    
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = url;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.closeModal('share')
    this.toastSvc.sendMessage("Copied!")
  }

  

  openModal(name){
    this.modal[name]=true
    
  }

  closeModal(name){
    this.modal[name] = false
  }
  

}
