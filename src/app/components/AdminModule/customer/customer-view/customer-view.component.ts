import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.css']
})
export class CustomerViewComponent implements OnInit {

  
customerForm = new FormGroup({
  business_id: new FormControl(null),
  customer_id: new FormControl(null),

  first_name: new FormControl(null, {
    validators: [Validators.required]
  }),

  last_name: new FormControl(null, {
    validators: [Validators.required]
  }),

  phone: new FormControl(null, {
    validators: [Validators.required]
  }),

  email: new FormControl(null, {
    validators: [Validators.required]
  }),




})
  business_id: string;
  customer_id: string;
  isLoading: boolean;
  customer: any;
  constructor(
    
  private route: ActivatedRoute,
  private customerService : CustomerService,
  ) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      this.customer_id = params.get("id")
      this.customerForm.controls['business_id'].setValue( this.business_id);
      if(this.customer_id){
        
        this.getCustomerDetails(this.business_id,this.customer_id)
      }
      
    })


  }


  async getCustomerDetails(business_id,customer_id){
    let isAdmin=1
   
    try{
      this.isLoading=true
      let response = await this.customerService.getCustomerDetails(business_id,customer_id)  
      this.customer=response.customer
      console.log(this.customer)
      this.customerForm.patchValue(this.customer)
      this.isLoading=false
   
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }

  async updateCustomer(){
    //console.log(this.customerForm.value)
  
    try{
      this.isLoading=true
      let response = await this.customerService.updateCustomer(this.customerForm.value)  
      console.log(response)
      this.customer=response.customer
      this.customerForm.patchValue(this.customer)
   
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }

}
