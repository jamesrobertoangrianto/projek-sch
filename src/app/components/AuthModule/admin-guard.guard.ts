import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AdminAuthService } from 'src/app/services/adminAuth/admin-auth.service';
import { RouterextService } from 'src/app/services/routerext/routerext.service';


@Injectable({
  providedIn: 'root'
})


export class AdminAuthGuard implements CanActivate {
  customerSession: any;
  constructor(private router: Router,
    public adminAuthService:AdminAuthService,
    ){
      
    }

 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let res = this.adminAuthService.getLocalCustomerSession()
      if (res!==null) {
          // logged in so return true
          return true;
      }

      // not logged in so redirect to login page with the return url and return false
      this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url }});
      return false;
  }

      
}