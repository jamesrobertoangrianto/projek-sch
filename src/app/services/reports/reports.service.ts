import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  BaseURL = environment.BaseURL
  constructor() { }

  async getSales(id:string,status:any){
  
    let res = await fetch(this.BaseURL+'reports/viewsales/business_id/'+id+'/status/'+status,{
      method: "GET",
      
    })
    let data = await res.json()
    return data
  }

  async getCustomer(id:string){
  
    let res = await fetch(this.BaseURL+'reports/viewcustomer/business_id/'+id,{
      method: "GET",
      
    })
    let data = await res.json()
    return data
  }
  async getProduct(id:string,status:any){
  
    let res = await fetch(this.BaseURL+'reports/viewproduct/business_id/'+id+'/status/'+status,{
      method: "GET",
      
    })
    let data = await res.json()
    return data
  }
}
