import { TestBed } from '@angular/core/testing';

import { FcmWiaService } from './fcm-wia.service';

describe('FcmWiaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FcmWiaService = TestBed.get(FcmWiaService);
    expect(service).toBeTruthy();
  });
});
