import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FcmWiaService {
  private all='all'

  private link = 'https://wia.id/wia-module/notification'
  private authorization = "key=AIzaSyDPLB-HSYQmu_PeRqII0Mxwy6DDrlCLWsc"

  private kHttpHeaders = new HttpHeaders({
    "Content-Type":"application/json",
    "Authorization": this.authorization
  })

  constructor() {}

  registerFCMToFirebaseTopic(fcm, topic){
    fetch("https://iid.googleapis.com/iid/v1/"+fcm+"/rel/topics/"+topic,{
      method: "POST",
      headers: {
        "Content-Type":"application/json",
        "Authorization": this.authorization
      }
    }).then(
      (res)=>{
        console.log("register topic all success")
      },
    ).catch((err)=>{
      console.log("register topic all fail")
    })
  }

  registerFCMToWia(fcm){
    fetch(this.link,{
      method: "POST",
      body: JSON.stringify({
        "fcm_id": fcm,
      })
    }).then(
      (res)=> res.json()
    ).then(
      (json)=>{
        console.log("register new fcm success!")
        console.log(json)
      }
    ).catch(
      (err) => {
        if(err.status == 200) console.log("Tadi error palsu")
        else console.log(err)
      }
    )
  }

  updateFCM(newFcm){
    fetch(
      this.link+`?fcm_id=${this.getCookie('wiaFcmToken')}`,{
        method: 'PUT',
        body: JSON.stringify({
          "fcm_id": newFcm,
        })
      }
    ).then(
      (res)=>{
        res.json()
      }
    ).then(
      (json) => {
        console.log("update new fcm success")
        console.log(json)
      }
    ).catch(
      (err) => console.log(err)
    )
  }

  getCookie(cname){
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return null;
  }
  
}
