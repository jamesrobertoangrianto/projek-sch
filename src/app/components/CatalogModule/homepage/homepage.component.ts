import { Component, OnInit } from '@angular/core';
import { ToastService } from 'src/app/services/global/toast.service';
import { Router } from '@angular/router';
import { AdminAuthService } from 'src/app/services/adminAuth/admin-auth.service';
import { faBars } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {


  faBarss=faBars
  
  isLogin: boolean;
  business_id: any;


  constructor(

    public toastService: ToastService,
    public router: Router,
    public adminAuthService:AdminAuthService
  ) { }

  async ngOnInit() {

    this.isLogin=false
    this.business_id=false
   
    try {
      let res = this.adminAuthService.getLocalCustomerSession()
      if(res.customer.isLogin){
        this.isLogin=true
        this.business_id=res.customer.business_id
      }
    } catch (error) {
      
    }
   
    
  }





}
