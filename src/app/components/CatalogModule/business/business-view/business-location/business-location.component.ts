import { Component, OnInit, Input } from '@angular/core';
import { faEnvelope,faEdit, faMapPin, faPhone,faGlobe } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-business-location',
  templateUrl: './business-location.component.html',
  styleUrls: ['./business-location.component.css']
})
export class BusinessLocationComponent implements OnInit {
  faPhone=faPhone
  faEdit=faEdit
  faGlobes=faGlobe
  faMapPin=faMapPin
  faEnvelope=faEnvelope
  @Input() business : any
  constructor() { }

  ngOnInit() {
  }

}
