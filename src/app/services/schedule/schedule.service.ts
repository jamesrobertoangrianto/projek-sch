import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  BaseURL = environment.BaseURL
  constructor() { }
  


  
  async getScheduleList(id:string,params:string){
    
    let res = await fetch(this.BaseURL+'schedule/view/business_id/'+id+params,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getScheduleById(id:string){
   
    let res = await fetch(this.BaseURL+'schedule/view/id/'+id,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }
}
