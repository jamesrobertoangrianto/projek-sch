import { Component, OnInit, OnChanges } from '@angular/core';
import { ToastService } from 'src/app/services/global/toast.service';
import { AdminAuthService } from 'src/app/services/adminAuth/admin-auth.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css'],
})
export class ToastComponent implements OnInit {
  msg: string
  actionName: string
  actionFn: VoidFunction
  show: boolean
  isFull : boolean = false

  timeout

  constructor(
    public toastService : ToastService,
    public adminAuthService:AdminAuthService
  ) { }

  ngOnInit() {
   
    
    this.toastService.launchEvent.subscribe(
      (res)=>{
        if(res == 'launch') {
          clearTimeout(this.timeout)
          this.isFull=this.toastService.isFull
          this.show=true
          this.msg = this.toastService.messages
          this.actionName = this.toastService.actionName
          this.actionFn = this.toastService.actionFn

          if(!this.isFull){
            this.timeout = setTimeout(()=>{
              this.show=false
              this.toastService.destroy()
            },3000)
          }
          

        }
      }
    )
  }

  executeFn(){
    this.show=false
    clearTimeout(this.timeout)
    this.toastService.actionFn()
    this.toastService.destroy()
  }

  dismiss(){
    this.show=false
    this.toastService.destroy()
    clearTimeout(this.timeout)
  }


  //   getCustomerSession(){
  //     this.adminAuthService.getCustomerSession()
     
  //  }

}
