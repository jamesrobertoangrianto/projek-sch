import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product/product.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/global/toast.service';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.css']
})
export class CategoryViewComponent implements OnInit {
tabs = [{
    id: '1',
    name: 'Details',

    // linkPath : '/admin/service',
  },
  {
    id: '2',
    name: 'Associate Product',

    // linkPath : '/admin/service',
  },

]
active_tab: string;
business_id: string;
isLoading: boolean;
product: any;
associateProduct : Array<String> = [];


category_id: string;
filterResult: any;
category: any;


categoryForm = new FormGroup({
  business_id: new FormControl(null),
  id: new FormControl(null),

  name: new FormControl(null, {
    validators: [Validators.required]
  }),

  short_description: new FormControl(null, {
    validators: [Validators.required]
  }),

  description: new FormControl(null),
  associate_product: new FormControl(null),
  
  status: new FormControl(null,{
    validators: [Validators.required]
  }),


})
  routerLink: string;



constructor(
  private productService: ProductService,
  private router: Router,
  private route: ActivatedRoute,
  private toastSvc: ToastService,
  
) {}


ngOnInit() {
  this.route.paramMap.subscribe(params => {
    this.business_id = params.get("business_id")
    this.categoryForm.controls['business_id'].setValue(this.business_id);
    this.getServiceList(this.business_id)
    this.routerLink='/admin/service/'+this.business_id

  })

  this.route.paramMap.subscribe(params => {
    this.category_id = params.get("id")
    this.getCategoryById(this.category_id)
    console.log(this.category_id)    
  })
  this.getActiveTab()



}


async getCategoryById(id){
  try{
    this.isLoading=true
    let response = await this.productService.getCategoryById(id)  
    this.category=response.category
    if(response.category.associate_product){
      this.associateProduct=this.category.associate_product
    }
    

    this.categoryForm.patchValue(this.category)

    console.log(this.associateProduct)
    this.isLoading=false
    
 
  }catch(e){
    console.log(e)
  }finally{
    this.isLoading=false
  }
}

toggleCheckbox(id,event){
  console.log(id,event.target.checked)

  this.categoryForm.controls[id].setValue(event.target.checked);
   
 }



getActiveTab() {
  this.route.queryParamMap.subscribe(queryParams => {
    let tab = this.route.snapshot.queryParamMap.get('active_tab')
    if (tab) {
      this.active_tab = tab
    } else {
      this.active_tab = '1'
    }
  })
}

updateSearchText(text: string) {
  let a =  text.toLowerCase()
  let res = this.product.filter(item => item.name === text);
  console.log(res)
  console.log(a)
  
  
}

checkSelectedAssociateProduct(item){
  if(this.associateProduct){
    return this.associateProduct.includes(item.id)
  }
 
// this.category.associate_product.filter(item => item.entity_id === item.id)
}

selectAssociateProduct(event) {
  console.log(event.target.checked)
  if (event.target.checked == true) {
    this.associateProduct.push(event.target.value);
  }
  if (event.target.checked == false) {
    let res = this.associateProduct.filter(i => i !== event.target.value)
    this.associateProduct = res
  }
  this.categoryForm.controls['associate_product'].setValue(this.associateProduct);
  console.log(this.associateProduct)

}



async getServiceList(id) {
  let isAdmin = 1
  try {
    this.isLoading = true
    let response = await this.productService.getProductList(id, 1)
    this.product = response.product
    console.log(this.product)

  } catch (e) {
    console.log(e)
  } finally {
    this.isLoading = false
  }
}




async updateCategory(){
  // console.log(this.categoryForm.value)  
  try{
      
    let res =  await this.productService.updateCategory(this.categoryForm.value)
   
    
    this.toastSvc.sendMessage("Update success!")

  }catch(e){
    // this.toastSvc.sendMessage(e)
  }finally{
    this.isLoading=false
    
  }
  
}





}
