import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/services/business/business.service';
import { ActivatedRoute } from '@angular/router';
import {  ToastService } from 'src/app/services/global/toast.service';
import {faUser, faPlus, faCalendar, faChevronRight, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { ReportsService } from 'src/app/services/reports/reports.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  isLoading: boolean;
  faCalendar=faCalendar
  faPlus=faPlus
  faUser=faUser
  business_id: string;
  customer: any;
  faChevronRight=faChevronRight
  faChevronDown=faChevronDown
  title='Dashboard'
  total_booking: any;
  sales: any;
  customerReports: any;
  salesReports: any;
  productReports: any;
  Homemenu: ({ title: string; subtitle: string; path: string; } | { title: string; subtitle: string; path?: undefined; })[];


  constructor(
    private businesService: BusinessService,
    private reportsService: ReportsService,
    private route: ActivatedRoute,
    private toastSvc: ToastService,
  ) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
      this.getBusinessProfile(this.business_id)
      this.getSalesReports(this.business_id)
      this.getCustomerReports(this.business_id)
      this.getProductReports(this.business_id)
    })
    this.getDate()


  }

  async getSalesReports(business_id) {
    try {
      this.isLoading = true
      let response = await this.reportsService.getSales(business_id, 'processing')
      console.log(response)
      this.salesReports = response
    } catch (e) {
      console.log(e)
    } finally {
      this.isLoading = false
    }
  }


  async getCustomerReports(business_id) {
    try {
      this.isLoading = true
      let response = await this.reportsService.getCustomer(business_id)
      console.log(response)
      this.customerReports = response

    } catch (e) {
      console.log(e)
    } finally {
      this.isLoading = false
    }
  }


  async getProductReports(business_id) {
    try {
      this.isLoading = true
      let response = await this.reportsService.getProduct(business_id, 1)
      console.log(response)
      this.productReports = response

    } catch (e) {
      console.log(e)
    } finally {
      this.isLoading = false
    }
  }


  async getBusinessProfile(business_id) {
    try {
      this.isLoading = true
      let response = await this.businesService.getBusinessProfile(business_id)
      console.log(response)
      this.customer = response.business.customer
    } catch (e) {
      console.log(e)
    } finally {
      this.isLoading = false
    }
  }

  getDate() {
    var date = new Date();
    return date
  }



  

}
