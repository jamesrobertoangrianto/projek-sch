import { Component, OnInit } from '@angular/core';
import { BookingService } from 'src/app/services/booking/booking.service';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/services/global/toast.service';
import { faChevronDown, faPhone,faEnvelope,faCalendarWeek } from '@fortawesome/free-solid-svg-icons';
import { formatDate } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-booking-view',
  templateUrl: './booking-view.component.html',
  styleUrls: ['./booking-view.component.css']
})
export class BookingViewComponent implements OnInit {
  isLoading: boolean;
  booking: any;
  title: string;
  faPhone=faPhone
  faEnvelopes=faEnvelope
  faChevronDown=faChevronDown


  modal={
    selectTime:false,
    selectDate:false,
   
  }
  time: any[];
  from_time: any;
  from_date: any;
  isReschedule: boolean;

  bookingForm = new FormGroup({
    id: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    business_id: new FormControl(null),

    from_date: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    from_time: new FormControl(null,{ 
      validators: [Validators.required]
    }),

   
  })
  business_id: string;
  routerLink: string;
  


  constructor( private bookingService : BookingService,
    private route: ActivatedRoute,
    private toastSvc: ToastService,

    ) { }

  ngOnInit() {
    this.isReschedule = false

    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      this.routerLink='/admin/booking/'+this.business_id
  
      
    })

   
    
     let id = this.route.snapshot.paramMap.get("id")
      console.log(id)
     this.getBookingById(id)
  }

  async getBookingById(id){
    try{
      this.isLoading=true
      let response = await this.bookingService.getBookingById(id)  
      this.booking=response.booking
      console.log(response)
      this.isLoading=false
      this.title='#'+response.booking.increment_id;

      this.bookingForm.controls['id'].setValue(response.booking.id);
    

     
    }catch(e){
      console.log(e)
    }finally{
     
    }
  }


  async updateStatus(id,status){
    
    try{
     let response = await this.bookingService.changeStatus(id,status)  
      console.log(response)
      //this.booking=response.booking
      this.booking.status=response.booking.status
      this.booking.status_label=response.booking.status_label

      if(this.booking.status=='processing'){
        this.toastSvc.sendMessage("Added to you schedule!")
      
      }
       
      }catch(e){
      console.log(e)
    }finally{
     
    }
  }


  reschedule(){
    this.isReschedule = true
    this.showTime()
   
  }

  async updateSchedule(){


    try{
      let response = await this.bookingService.updateSchedule(this.bookingForm.value)  
       console.log(response)
       this.isReschedule = false
       this.toastSvc.sendMessage("Schedule Update!")
     
        
       }catch(e){
       console.log(e)
     }finally{
      
     }
    
  }


  openModal(name){
    this.modal[name]=true
    
  }

  closeModal(name){
    this.modal[name] = false
  }

    
  showTime(){
    var today = new Date();
    var currentMonth = today.getMonth();
    var currentYear = today.getFullYear();

    var openTime = new Date(today.setHours(10,0,0))
    var closeTime = new Date(today.setHours(20,0,0))

    var arr = new Array();
    var dt = new Date(openTime);
    while (dt <= closeTime) {
        arr.push(new Date(dt));
        dt.setHours(dt.getHours() + 1);
    }
    this.time=arr
    console.log( arr)

  }


  selectTime(time){
    
    this.booking.from_time=formatDate(time, 'HH:mm', 'en_US')
    console.log(this.from_time)
    this.bookingForm.controls['from_time'].setValue(this.booking.from_time);
    
    this.closeModal('selectTime')
  }

  
  selectDate(date){
    this.booking.from_date= formatDate(date, 'yyyy-MM-dd', 'en_US') 
    console.log(this.from_date)
    this.bookingForm.controls['from_date'].setValue(this.booking.from_date);
   
    this.closeModal('selectDate')
  }


}
