import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  BaseURL = environment.BaseURL
  constructor() { }
  
  
  async getProductList(id:string,status:any){
  
    let res = await fetch(this.BaseURL+'product/view/business_id/'+id+'/status/'+status,{
      method: "GET",
      
    })
    let data = await res.json()
    return data
  }

  async getCategoryList(id:string,status:any){
  
    let res = await fetch(this.BaseURL+'product/viewcategory/business_id/'+id+'/status/'+status,{
      method: "GET",
      
    })
    let data = await res.json()
    return data
  }




  async getProductById(id:string){
    
    let res = await fetch(this.BaseURL+'product/view/id/'+id,{
      method: "GET",
     
    })
    let data = await res.json()
    return data
  }

  async getCategoryById(id:string){
    
    let res = await fetch(this.BaseURL+'product/viewcategory/id/'+id,{
      method: "GET",
     
    })
    let data = await res.json()
    return data
  }



  async addProduct(id){
    let res = await fetch(this.BaseURL+'product/add/business_id/'+id,{
      method: "POST",
     
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }

  
  async addCategory(id){
    let res = await fetch(this.BaseURL+'product/addcategory/business_id/'+id,{
      method: "POST",
     
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }

  async removeProduct(business_id,product_id){
    
    let res = await fetch(this.BaseURL+'product/delete/business_id/'+business_id+'/product_id/'+product_id,{
      method: "POST",
     
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }



  async updateProduct(formvalue){
    let res = await fetch(this.BaseURL+'product/edit/',{
      method: "POST",
      body: JSON.stringify(formvalue)
     
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }

  async updateCategory(formvalue){
   
    let res = await fetch(this.BaseURL+'product/editcategory/',{
      method: "POST",
      body: JSON.stringify(formvalue)
     
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }

  async updateAssociateProduct(formvalue){
    console.log(formvalue)
    // let res = await fetch(this.BaseURL+'product/edit/',{
    //   method: "POST",
    //   body: JSON.stringify(formvalue)
     
    // })
    // let data = await res.json()
    // if(data.status_code != 200) throw data.message_dialog
    // else{
    //   return data
    // }
  }

  
}
