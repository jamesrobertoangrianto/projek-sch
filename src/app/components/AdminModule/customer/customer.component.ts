import { Component, OnInit } from '@angular/core';
import {faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/services/customer/customer.service';


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  faChevronRight=faChevronRight
  title='Customer'
  isLoading: boolean;
  business_id: any;
  customer: any;
  constructor(
    private router : Router,
    
    private route: ActivatedRoute,
    private customerService : CustomerService,


  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      this.getCustomerList(this.business_id)
    })
    
  }

  updateSearchText(text: string) {
    
  }

  async getCustomerList(id){
    let isAdmin=1
    try{
      this.isLoading=true
      let response = await this.customerService.getCustomerList(id)  
      this.customer=response.customers
      console.log(this.customer)
      this.isLoading=false
   
    }catch(e){
      console.log(e)
    }finally{
      
    }
  }



  viewCustomerDetails(id) {
    this.router.navigate(['/admin/customer/'+this.business_id+'/view/customer_id/'+id]);
  }
  

}
