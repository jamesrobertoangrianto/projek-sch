import { Injectable } from '@angular/core';

declare var gtag: any
declare var fbq: any

type Item = {
  item_id: string,
  product_id: string,
  name: string,
  brand: string,
  request_quantity: any,
  available_quantity: any,
  url_path: string,
  has_error: boolean,
  price: string,
}

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor() { }

  beginCheckout(cart: Item[]){
    console.log('Google: begin_checkout_tag')
    var cartTrack: any = cart
    gtag('event', 'begin_checkout', {
      items: cartTrack
    })

    console.log('FB: begin_fb_checkout')
    var subtotal = 0
    cartTrack.forEach((item: Item)=>{
      subtotal+=item.request_quantity*parseInt(item.price)
    })
    fbq('track', 'InitiateCheckout',{
      value: subtotal+'.00',
      currency: 'IDR',
      contents: cartTrack.map((item: Item) =>{
        return  {
          'id': item.product_id, //product id
          'quantity': item.request_quantity
        }
      }),
      content_type: 'product'
    })

  }

  beginPayment(cart: Item[]){
    console.log('Google: begin_payment')
    var cartTrack: any = cart

    console.log(cartTrack)
    gtag('event', 'checkout_progress', {
      "items": cartTrack,
      "checkout_step": 1,
    });

    console.log('FB: begin_payment_fb')
    var subtotal = 0
    cartTrack.forEach((item: Item)=>{
      subtotal+=item.request_quantity*parseInt(item.price)
    })
    fbq('track', 'PaymentPage',{
      value: subtotal+'.00',
      currency: 'IDR',
      contents: cartTrack.map((item: Item) =>{
        return  {
          'id': item.product_id, //product id
          'quantity': item.request_quantity
        }
      }),
      content_type: 'product'
    })
  }
}
