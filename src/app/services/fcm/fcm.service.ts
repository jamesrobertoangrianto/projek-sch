import { Injectable } from '@angular/core';

type cartId = any | '0'
type customerID = any | '0'

@Injectable({
  providedIn: 'root'
})

export class FcmService {
  private url = 'https://wia.id/wia-module/notification'
  private fcmToken: string

  constructor() {
    this.fcmToken=this.getCookie("wiaFcmToken")
  }
  
  async setFCMToCart(cartID : cartId, customer_id?: customerID){
    console.log("set FCM to cart")
    if(this.fcmToken == null) {
      console.warn("This browser doesn't allow FCM! Canceling action")
      return
    }
    let body = {
      "fcm_id": this.fcmToken,
      "behavior": 'Cart',
      "cart_id": cartID,
      "customer_id": customer_id == null ? '0' : customer_id
    }
    return await this.updateFCM(body)
  }

  async setFCMToCheckout(cartID : cartId, customer_id?: customerID){
    console.log("set FCM to checkout")
    if(this.fcmToken == null) {
      console.warn("This browser doesn't allow FCM! Canceling action")
      return
    }
    let body = {
      "fcm_id": this.fcmToken,
      "behavior": 'Checkout',
      "cart_id": cartID,
      "customer_id": customer_id == null ? '0' : customer_id
    }
    return await this.updateFCM(body)
  }

  async resetFCM(){
    console.log("set FCM to none")

    if(this.fcmToken == null) {
      console.warn("This browser doesn't allow FCM! Canceling action")
      return
    }
    let body = {
      "fcm_id": this.fcmToken,
      "behavior": '',
      "cart_id": '0',
      "customer_id": '0'
    }
    return await this.updateFCM(body)
  }
  
  
  private async updateFCM(body: any){
    let res = await fetch(this.url+`?fcm_id=${this.fcmToken}`,{
      method: "PUT",
      body: JSON.stringify(body)
    })
    let data = await res.json()
    return data
  }

  private getCookie(cname){
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return null;
  }

}
