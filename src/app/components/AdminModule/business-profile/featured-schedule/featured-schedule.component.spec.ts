import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedScheduleComponent } from './featured-schedule.component';

describe('FeaturedScheduleComponent', () => {
  let component: FeaturedScheduleComponent;
  let fixture: ComponentFixture<FeaturedScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
