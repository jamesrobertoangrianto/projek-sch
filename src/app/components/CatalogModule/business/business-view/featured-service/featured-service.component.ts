import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-featured-service',
  templateUrl: './featured-service.component.html',
  styleUrls: ['./featured-service.component.css']
})
export class FeaturedServiceComponent implements OnInit {
  isLoading: boolean;
  product: any;

  @Output() onClick = new EventEmitter()

  constructor(
    private productService : ProductService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getServiceList()
  }

  Click(id: any) {
    this.onClick.emit(1);
   
  }

  async getServiceList(){
    try{
      this.isLoading=true
      let response = await this.productService.getProductList(null,null)  
      this.product=response.product

      // this.from_date=response.from_date
      console.log(this.product)
   
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }


}
