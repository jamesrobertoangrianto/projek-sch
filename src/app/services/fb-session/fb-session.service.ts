import { Injectable, EventEmitter } from '@angular/core';
import { ToastService } from '../global/toast.service';

declare var FB: any
export type FBLoginState = 'processing' | 'login' | 'logout'
@Injectable({
  providedIn: 'root'
})

export class FbSessionService {
  constructor(
    private toastSvc: ToastService,
   
  ) {}

  fbTokens = new EventEmitter(null)

  checkLoginState(){
    FB.getLoginStatus(res => this.statusChangeCallback(res))
  }

  statusChangeCallback(res){  // Called with the results from FB.getLoginStatus().
    if (res.status === 'connected') {   // Logged into your webpage and Facebook.
      this.fbTokens.next(res)
      this.getFacebookEmail()
    } else {                                 // Not logged into your webpage or we are unable to tell.
      this.fbTokens.next(null)
      console.warn("No logged in facebook session")
    }
  }

  loginByFacebook(){
    FB.login(res => {
      if (res.status === 'connected') {
        this.statusChangeCallback(res)
      } else {
        this.toastSvc.sendMessage("Login using facebook was cancelled")
      }
    },{
      scope: 'email,public_profile',
      return_scopes: true
    });
  }

  private getFacebookEmail(){
    FB.api('/me', {fields: 'email,first_name,last_name,picture,gender'}, (response)=>{
    });
  }

  async signInUsingFBToken(body){
    try{
      let res = await fetch('https://wia.id/wia-module/customer/facebooklogin',{
        method: "POST",
        body: JSON.stringify(body)
      })
  
      let data = await res.json()
      if(data.message_code == 200){
       
        return true
      }else{
        return false
      }
    }catch(e){
      console.warn("Api error on signin using fb token! ")
      console.warn(e)
      return false
    }
  }

}
