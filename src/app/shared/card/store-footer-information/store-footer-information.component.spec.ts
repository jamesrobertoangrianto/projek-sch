import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreFooterInformationComponent } from './store-footer-information.component';

describe('StoreFooterInformationComponent', () => {
  let component: StoreFooterInformationComponent;
  let fixture: ComponentFixture<StoreFooterInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreFooterInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreFooterInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
