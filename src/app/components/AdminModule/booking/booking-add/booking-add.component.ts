import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product/product.service';
import { BookingService } from 'src/app/services/booking/booking.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { formError, validPhoneNumber } from 'src/app/const/formError';
import { ToastService } from 'src/app/services/global/toast.service';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { formatDate } from '@angular/common';



@Component({
  selector: 'app-booking-add',
  templateUrl: './booking-add.component.html',
  styleUrls: ['./booking-add.component.css']
})
export class BookingAddComponent implements OnInit {
  active_step: string;
  product_id: string;
  title: string;
  isLoading: boolean;
  product: any;
  formErrorMsg=formError
  
  faChevronRight=faChevronRight;

  modal={
    selectTime:false,
    selectDate:false,
   
  }
  

  bookingForm = new FormGroup({
    product_id: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    business_id: new FormControl(null),
    customer_email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email]
    }),
    customer_name: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    from_date: new FormControl(null,{ 
      validators: [Validators.required]
    }),
    from_time: new FormControl(null,{ 
      validators: [Validators.required]
    }),

    customer_phone: new FormControl(null,{ 
      validators: [Validators.required,validPhoneNumber]
    }),
   
  })
  isShowBookingForm: boolean;
  booking_id: string;
  isUpdate: boolean;
  business_id: string;
  from_time: any;
  from_date: string;
  time: any[];
  

  constructor(
    private productService : ProductService,
    private bookingService: BookingService,
    private route: ActivatedRoute,
    private router: Router,
    private toastSvc: ToastService,
  ) { }

  ngOnInit() {
    this.isShowBookingForm=true
    this.showTime()
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
      this.getServiceList(this.business_id)

      
    })

    this.route.queryParamMap.subscribe(queryParams => {
      this.active_step = queryParams.get("step")
      this.product_id = queryParams.get("product_id")
      this.setTitle(this.active_step)
      console.log(this.product_id)
      
      this.bookingForm.controls['product_id'].setValue(this.product_id);
      this.bookingForm.controls['business_id'].setValue(this.business_id);
     // this.product_id = queryParams.get("product_id")
   
    })
   
  }

 
  selectTime(time){
    
    this.from_time=formatDate(time, 'HH:mm', 'en_US')
    console.log(this.from_time)
    this.bookingForm.controls['from_time'].setValue(this.from_time);
    this.closeModal('selectTime')
  }

  
  selectDate(date){
    this.from_date= formatDate(date, 'yyyy-MM-dd', 'en_US') 
    console.log(this.from_date)
    this.bookingForm.controls['from_date'].setValue(this.from_date);
    this.closeModal('selectDate')
  }

  
  showTime(){
    var today = new Date();
    var currentMonth = today.getMonth();
    var currentYear = today.getFullYear();

    var openTime = new Date(today.setHours(10,0,0))
    var closeTime = new Date(today.setHours(20,0,0))

    var arr = new Array();
    var dt = new Date(openTime);
    while (dt <= closeTime) {
        arr.push(new Date(dt));
        dt.setHours(dt.getHours() + 1);
    }
    this.time=arr
    console.log( arr)

  }



  async getServiceList(id){
    try{
      this.isLoading=true
      let response = await this.productService.getProductList(id,1)  
      this.product=response.product

      // this.from_date=response.from_date
      console.log(this.product)
   
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }


  setTitle(step){
    if(step == 1 ){
      this.title = 'Select a Service'
    }
    if(step == 2 ){
      this.title = 'Pick a Staf'
    }
    if(step == 3 ){
      this.title = 'Add Customer Details'
    }
  }


  //step 2
  async addBooking(){
     //alert('123')
    if(this.bookingForm.valid){
      try{
        this.isUpdate=true
        
        let response = await this.bookingService.addBooking(this.bookingForm.value)
        console.log(response)
        this.booking_id=response.booking.id

        this.isUpdate=false

        this.toastSvc.sendMessage("New Order Added!")
        this.router.navigate([ '/admin/booking/'+this.business_id+'/view/booking_id/'+  this.booking_id],{replaceUrl:true})
        
        //this.isShowBookingForm=false
       
    
      }catch(e){
        console.log(e)
      }finally{
        
      }
    }
    else{
      this.toastSvc.sendMessage("Please check your booking details!")
    }

      
      
    
  }

  openModal(name){
    this.modal[name]=true
    
  }

  closeModal(name){
    this.modal[name] = false
  }



}
