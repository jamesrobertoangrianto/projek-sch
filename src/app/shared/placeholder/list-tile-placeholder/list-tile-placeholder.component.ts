import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-tile-placeholder',
  templateUrl: './list-tile-placeholder.component.html',
  styleUrls: ['./list-tile-placeholder.component.css']
})
export class ListTilePlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
