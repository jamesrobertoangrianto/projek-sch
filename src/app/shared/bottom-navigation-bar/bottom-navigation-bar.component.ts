import { Component, OnInit, Input } from '@angular/core';
import {  faThList, faStore, faCalendarWeek, faReceipt ,faPercent,faThLarge,faUserCircle,faHome,faTags, faFileInvoiceDollar } from '@fortawesome/free-solid-svg-icons';
import { AdminAuthService } from 'src/app/services/adminAuth/admin-auth.service';

@Component({
  selector: 'app-bottom-navigation-bar',
  templateUrl: './bottom-navigation-bar.component.html',
  styleUrls: ['./bottom-navigation-bar.component.css']
})
export class BottomNavigationBarComponent implements OnInit {
  faUserCircle = faUserCircle
  faHome = faHome
  faStore=faStore
  faThList=faThList
  faFileInvoiceDollar = faFileInvoiceDollar
  faTags= faTags
  faCalendarWeek=faCalendarWeek
  faReceipt = faReceipt
  faThLarge= faThLarge
  faPercent =faPercent

  

  @Input() isAdmin?: boolean
  customer_id: any;
  business_id: any;

  constructor(
    private adminAuthService :AdminAuthService
  ) { }

  ngOnInit() {

 

    let res = this.adminAuthService.getLocalCustomerSession()
    console.log(res.customer.business_id)
    this.business_id=res.customer.business_id
 
 
  }

}
