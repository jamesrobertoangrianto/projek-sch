import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, UrlSerializer } from '@angular/router';
import { BookingService } from 'src/app/services/booking/booking.service';
import {faChevronRight, faCircle, faCalendarWeek,faUser} from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  title = 'Booking'

  faCalendarWeek=faCalendarWeek
  faUser=faUser
  faCircle=faCircle
  faChevronRight=faChevronRight

  currentUrl: string;
  activeTab: string;
  isLoading: boolean;
  booking: any;
  from_date: any;
  tab_id: string;
  business_id: string;

  params: string;
  tabs: { tab: string; key: string; value: string; linkPath: string; }[];
  search: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private serializer: UrlSerializer,
    private bookingService : BookingService) { 
    
  }

  ngOnInit() {

    this.search=''
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
     
    })

    this.route.queryParams.subscribe(params => {
     if(params.increment_id){
      this.search = params.increment_id
     }
     

      this.params = this.serializer.serialize(this.router.createUrlTree([''], { queryParams: params}))
      
      this.getBookingList(this.business_id,this.params)
      
     
    })

    this.setTab()
  }

  setTab(){
    this.tabs = [
      {
        tab : 'New Request',
        key : 'Status',
        value : 'pending',
        linkPath : '/admin/booking/'+this.business_id,
        
      },
      // {
      //   tab : 'confirm',
      //   linkPath : '/admin/booking/'+this.business_id,
      // },
      // {
      //   tab : 'canceled',
      //   linkPath : '/admin/booking/'+this.business_id,
      // },
     
    ]
  }

   
  async getBookingList(id,params){
    try{
      this.isLoading=true
      let response = await this.bookingService.getBookingList(id,params)  
      this.booking=response.booking
      this.from_date=response.from_date
      console.log(response)
      this.isLoading=false
    }catch(e){
      console.log(e)
    }finally{
     
    }
  }

  updateSearchText(increment_id: string) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { increment_id },
      queryParamsHandling: "merge",
    });
  }




}
