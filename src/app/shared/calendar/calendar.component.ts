import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faChevronRight,faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  calendar: any[];
  currentMonth: number;
  currentYear: number;

  faChevronRight=faChevronRight
  faChevronCircleLeft=faChevronLeft

  selectedDate: any;
  @Output() onSelect = new EventEmitter()
  selectedMonth: Date;
  days=[ 'Sun','Mon','Tue','Wed','Thu','Fri','Sat']

  constructor() { }

  ngOnInit() {
    this.selectedDate=false
    const today = new Date();
    this.currentMonth = today.getMonth();
    this.currentYear = today.getFullYear();
    this.showCalendar(this.currentYear,this.currentMonth)

  }

  showCalendar(currentYear,currentMonth){

    var startDate = new Date(currentYear,currentMonth);
    var endDate= this.getEndDate(currentYear,currentMonth);
    var startDay = startDate.getDay();  
    this.selectedMonth=startDate
   
    var calendar =new Array()
  
    for (let i = 0; i < startDay ; i++) {
      var date = new Date(startDate);
      calendar.push({
        'date' : null,
      })
    }

    for (let i = 0; i < endDate ; i++) {
      var date = new Date(startDate);
      var storeDate = new Date(date.setDate(date.getDate() + i ))
      var enable = true

      calendar.push({  
        'date' : storeDate,
        'selected':false,
        'available':this.checkAvailability(storeDate),
        'enable': enable? enable: false,
      })
    }

    this.calendar=calendar
  }

  

  getEndDate(iYear,iMonth) 
  { 
      return 32 - new Date(iYear, iMonth, 32).getDate();
  }


  next() {
    this.currentYear = (this.currentMonth === 11) ? this.currentYear + 1 : this.currentYear;
    this.currentMonth = (this.currentMonth + 1) % 12;
    this.showCalendar(this.currentYear,this.currentMonth);
  }

  previous() {  
    this.currentYear = (this.currentMonth === 11) ? this.currentYear - 1 : this.currentYear;
    this.currentMonth = (this.currentMonth - 1) % 12;
    this.showCalendar(this.currentYear,this.currentMonth);
  }

  selectDate(i,item){
    if(item.available){
      this.selectedDate=item.date
      this.onSelect.emit(this.selectedDate)
      console.log(this.selectedDate)
    }
  }

  selectDates(i,date){
    this.calendar[i].selected=!this.calendar[i].selected
  }

  checkAvailability(storeDate){
    const today = new Date();
    var availableDate= new Date("March 27, 2020 00:00:00");
    var favailableDate = formatDate(availableDate, 'yyyy-MM-dd', 'en_US') 

    if(formatDate(storeDate, 'yyyy-MM-dd', 'en_US') < formatDate(today, 'yyyy-MM-dd', 'en_US') ){
      return false
    }

    if(favailableDate === formatDate(storeDate, 'yyyy-MM-dd', 'en_US') ){
      return true
    }
    else{
      return true
    }
  }



}
