import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthService {
  BaseURL = environment.BaseURL
  customerSession: any;
  constructor() { }
  
  
  // async getCustomerSession(){
  //   let res = await fetch(this.BaseURL+'customer/getCutomerSession',{
  //     method: "GET"
  //   })  
  //   let data = await res.json()
   
  //   this.setCustomerSession(data)
  //   //console.log(data)
  // }

  setCustomerSession(data){
    localStorage.setItem('customerSession', JSON.stringify(data));
  }

  getLocalCustomerSession(){
    let item = JSON.parse(localStorage.getItem('customerSession'));
    return item;
  }

  async login(loginInfo:{email: string, password: string}){
    let res = await fetch(this.BaseURL+'customer/login',{
      body: JSON.stringify(loginInfo),
      method: "POST"
    })
    let data = await res.json()
    
    if(data.message_code != 200) throw data.message_dialog
    else{
      this.setCustomerSession(data)
      return data
    }
  }

  async logout(){
    let res = await fetch(this.BaseURL+'/customer/logout')
    let data = await res.json()
    this.setCustomerSession('')

    if(data.message_code != 200) throw data.message_dialog
    else{
      return true
    }
  }

  async register(loginInfo){
    let res = await fetch(this.BaseURL+'customer/add',{
      body: JSON.stringify(loginInfo),
      method: "POST"
    })
    let data = await res.json()
    
   // console.log(data)
    if(data.message_code != 200) throw data.message_dialog
    else{
      this.setCustomerSession(data)
      return data
    }
  }




}
