import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FbSessionService, FBLoginState } from 'src/app/services/fb-session/fb-session.service';
import { ToastService } from 'src/app/services/global/toast.service';

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.css']
})
export class SocialLoginComponent implements OnInit {
  disableFBButton: boolean
  @Output() onSuccessFacebookLogin = new EventEmitter()
  
  constructor(
    private fbSesSvc: FbSessionService,
    private toastSvc: ToastService
  ) { }

  ngOnInit() {}

  async loginFB(){
    this.fbSesSvc.fbTokens.subscribe(
      async (t)=>{
        if(t){
          let res = await this.fbSesSvc.signInUsingFBToken({
            token: t.authResponse.accessToken
          })
          if(res){
            this.toastSvc.sendMessage("Login with facebook was successfull!")
            this.onSuccessFacebookLogin.emit(true)
          }else{
            this.onSuccessFacebookLogin.emit(false)
          }
        }
      }
    )
    
    this.fbSesSvc.loginByFacebook()
  }

}
