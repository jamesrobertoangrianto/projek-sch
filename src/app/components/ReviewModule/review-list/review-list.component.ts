import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReviewService } from 'src/app/services/review/review.service';
import { Reviews } from 'src/app/model/review';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.css']
})
export class ReviewListComponent implements OnInit {
  private productID;

  isLoading: boolean

  reviews: Reviews

  constructor(
    private activatedRoute: ActivatedRoute,
    private reviewSvc: ReviewService
  ) { }

  async ngOnInit() {
    this.productID=this.activatedRoute.snapshot.params.id
    await this.loadReviews()
    window.scroll(0,0);
  }

  async loadReviews(){
    try{
      this.isLoading=true
      this.reviews = await this.reviewSvc.getReviewsByProductID(this.productID)
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }
}
