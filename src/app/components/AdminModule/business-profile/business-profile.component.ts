import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/services/business/business.service';
import { faEdit, faMapPin, faPhone,faGlobe } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { formError, validPhoneNumber } from 'src/app/const/formError';
import { ToastService } from 'src/app/services/global/toast.service';


@Component({
  selector: 'app-business-profile',
  templateUrl: './business-profile.component.html',
  styleUrls: ['./business-profile.component.css']
})
export class BusinessProfileComponent implements OnInit {
  isLoading: boolean;
  business: any;
  formErrorMsg = formError

  faPhone=faPhone
  faEdit=faEdit
  faGlobes=faGlobe
  faMapPin=faMapPin
  business_id: any;
  title='Business Profile'

  modal={
    updateBusiness:false,
    editAddress: false
  }

  businessForm = new FormGroup({
    id: new FormControl(null),
    business_owner_id: new FormControl(null),
   
    name: new FormControl(null,{
      validators: [Validators.required]
    }),
    description: new FormControl(null,{
      validators: [Validators.required]
    }),
    short_description: new FormControl(null,{
      validators: [Validators.required]
    })
  })

  addressForm = new FormGroup({
    business_id: new FormControl(null),
    id: new FormControl(null),

    city: new FormControl(null,{
      validators: [Validators.required]
    }),
    email: new FormControl(null,{
      validators: [Validators.required]
    }),
    region: new FormControl(null,),
    
  
    telephone: new FormControl(null,{
      validators: [Validators.required]
    }),

    website: new FormControl(null,{
      validators: [Validators.required]
    }),
  
    street: new FormControl(null,{
      validators: [Validators.required]
    })
  })


  constructor(
    private businesService: BusinessService,
    private route: ActivatedRoute,
    private toastSvc: ToastService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
      this.getBusinessProfile( this.business_id)
     
    })
   
  }

 
  async getBusinessProfile(business_id){
    try{
      this.isLoading=true
      let response = await this.businesService.getBusinessProfile(business_id)  
      this.business=response.business
      console.log(this.business)
      this.businessForm.patchValue(this.business)
      this.addressForm.patchValue(this.business.address)
     // this.getBusinessProfileGallery(this.business.id)
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }

  
  // async getBusinessProfileGallery(business_id){
  //   try{
  //     this.isLoading=true
  //     let response = await this.businesService.getBusinessProfileGallery(business_id)  
  //    this.business.photos=response.gallery
        
  //   }catch(e){
  //     console.log(e)
  //   }finally{
  //     this.isLoading=false
  //   }
  // }




  async updateAddress(){
    try{
      this.isLoading=true
      let res =  await this.businesService.updateAddress(this.addressForm.value)
     
      
     console.log(res)  
     this.business.address=this.addressForm.value
     this.toastSvc.sendMessage("Update success!")
    
     this.closeModal('editAddress')
    
    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.isLoading=false
      
    }
  }


  async updateBusiness(){
    try{
      this.isLoading=true
      await this.businesService.updateBusiness(this.businessForm.value)
      this.business.name=this.businessForm.value.name
      this.business.description=this.businessForm.value.description
      this.business.short_description=this.businessForm.value.short_description
      
      
      this.closeModal('updateBusiness')
    
    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.isLoading=false
      
    }
  }



  openModal(name){
    this.modal[name]=true
    
  }

  closeModal(name){
    this.modal[name] = false
  }
  

}
