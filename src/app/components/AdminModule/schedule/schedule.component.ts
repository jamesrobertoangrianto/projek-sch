import { Component, OnInit } from '@angular/core';
import { ScheduleService } from 'src/app/services/schedule/schedule.service';
import { Router, ActivatedRoute, UrlSerializer } from '@angular/router';
import { formatDate } from '@angular/common';
import {faCircle, faThList, faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  title='Calendar'
  faUser=faUser
  faCircle=faCircle


  tabs = [
    {
      tab : 'Appointment',
      linkPath : '/admin/schedule',
    },
   
   
  ]
  isLoading: boolean;
  faThList=faThList
  schedule: any;
  total_customer: void;
  business_id: string;
  request_date: any;
  active_step: string;
  params: any;
  calendar: any;
  active: boolean;
  from_date: any;
  test: number;
  selectedDate: string;
  
  

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private serializer: UrlSerializer,
    private scheduleService : ScheduleService) { 
    
  }

  ngOnInit() {

  

    this.route.paramMap.subscribe(params => {
      this.business_id = params.get("business_id")
      console.log(this.business_id)
      
    })

    this.route.queryParams.subscribe(params => {
     this.from_date = new Date(params.from_date).toUTCString;


      this.params = this.serializer.serialize(this.router.createUrlTree([''], { queryParams: params}))
      console.log(this.params);
      this.getScheduleList(this.business_id,this.params)
     
    })

    this.showDate()
    
  }

  showDate(){
    var d = new Date();
    var startDate = new Date(); //YYYY-MM-DD
    var endDate = new Date(d.setDate(d.getDate()+6)); //YYYY-MM-DD

    var arr = new Array();
        var dt = new Date(startDate);
        while (dt <= endDate) {
            arr.push(new Date(dt));
            dt.setDate(dt.getDate() + 1);
        }
        //return arr;

   // var dateArr = getDateArray(startDate, endDate);
  //console.log(endDate)

    this.calendar=arr
    //console.log(this.calendar)
  
  }



  setDate(id){
    let date= formatDate(id, 'yyyy-MM-dd', 'en_US')
    console.log(date)
    this.selectedDate =  date
    this.router.navigate(['/admin/schedule/'+this.business_id ],{ queryParams: { from_date: date } })
     
  }

  getSelectedDate(item){
    let date= formatDate(item, 'yyyy-MM-dd', 'en_US')
    if(date == this.selectedDate )
    return true
  }
  


  viewBooking(id){
    this.router.navigate(['/admin/booking/'+this.business_id+'/view/booking_id/'+id])
     
  }

  async getScheduleList(id,params){
    try{
      this.isLoading=true
      let response = await this.scheduleService.getScheduleList(id,params)  
      this.schedule = response.schedule
      console.log(response)
      this.selectedDate =   formatDate(response.from_date, 'yyyy-MM-dd', 'en_US')
   
    }catch(e){
      console.log(e)
    }finally{
      this.isLoading=false
    }
  }


}
